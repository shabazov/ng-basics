import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { delay, catchError, map, tap } from 'rxjs/operators'

export interface Todo {
  completed: boolean;
  title: string;
  id?: number;
}

@Injectable({
  providedIn: 'root'
})

export class TodosService {
  constructor(private http: HttpClient) {}

  fetchTodos(): Observable<Todo[]> {
    let params = new HttpParams();
    params = params.append('_limit', '4');
    params = params.append('custom', 'any')

    return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos', {
      // params: new HttpParams().set('_limit', '3')
      params,
      observe: 'response'
    })
      .pipe(
        map(response => {
          console.log('response', response);
          
          return response.body
        }),
        delay(1500),
        catchError(error => {
          console.log('error', error.message);
          return throwError(error);
        })
      )    
  };

  addTodo(todo: Todo): Observable<Todo> {
    const headers = new HttpHeaders({
      myHeader: Math.floor(Math.random() * 10).toString()
    });
    return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', todo, {
      headers
    })
  };

  removeTodo(id: number): Observable<any> {
    return this.http.delete<void>(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      observe: 'events'
    }).pipe(
      tap(event => {
        if (event.type === HttpEventType.Sent) {console.log('sent', event)};
        if (event.type === HttpEventType.Response) {console.log('response', event)}
        
      })
    )
  };

  completeTodo(id: number): Observable<Todo> {
    return this.http.put<Todo>(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      comleted: true
    }, {
      responseType: 'json' 
    })
  }
}