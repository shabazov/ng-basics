import { NoopInterceptor } from './noop.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Provider } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { IfnotDirective } from './directives/ifnot.directive';
import { Style2Directive } from './directives/style2.directive';
import { Style3Directive } from './directives/style3.directive';
import { ReversePipe } from './pipes/reverse.pipe';


const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  useClass: NoopInterceptor,
  multi: true
}

@NgModule({
  declarations: [
    AppComponent,
    IfnotDirective,
    Style2Directive,
    Style3Directive,
    ReversePipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [INTERCEPTOR_PROVIDER],
  bootstrap: [AppComponent]
})
export class AppModule {}
