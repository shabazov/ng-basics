import { Directive, ElementRef, Renderer2, HostBinding, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appStyle2]'
})
export class Style2Directive {

  constructor(private el: ElementRef, private r: Renderer2) {
    this.el.nativeElement.style.background = 'teal'
   }

   @HostBinding('style.background') backColor = null
   @Input('appStyle2') color = ''
   @HostListener('mouseenter') onEnter() {
     this.backColor = this.color
   }
   @HostListener('mouseleave') onLeft() {
     this.backColor = null
   }

   @HostListener('click', ['$event.target']) changeColor(event: Event) {
      this.r.setStyle(this.el.nativeElement, 'color', 'red')
   }
   @HostListener('window:keydown.space') unsetColor(event: KeyboardEvent) {
     this.r.setStyle(this.el.nativeElement, 'color', null)
   }
}
