import { Directive, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appStyle3]'
})
export class Style3Directive {

  constructor(private el: ElementRef) { }

  possibleColors = [
    'darksalmon', 'hotpink', 'lightskyblue', 'goldenrod', 'peachpuff',
    'mediumspringgreen', 'cornflowerblue', 'blanchedalmond', 'lightslategrey'
  ];

  @HostBinding('style.background') color: string;
  @HostListener('window:keydown.space') setColor() {
    const colorPick = Math.floor(Math.random() * this.possibleColors.length)
    this.color = this.possibleColors[colorPick]
  }
  @HostListener('mouseleave') dropColor() {
    this.color = null
  }
  

}
