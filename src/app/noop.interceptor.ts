import { Observable } from 'rxjs';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpEvent, HttpEventType } from '@angular/common/http';
import { tap } from 'rxjs/operators';

export class NoopInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('intercept request', req);

    const cloned = req.clone({
      headers: req.headers.append('NU', 'ANOTHER HEADER'),
    })
    
    return next.handle(cloned)
      .pipe(
        tap(event => {
          if (event.type === HttpEventType.Response) {
            console.log('intercepted response', event);
          }
        })
        
    )
  }
}